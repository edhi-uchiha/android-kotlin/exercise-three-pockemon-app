package com.enigmacamp.pockemon

import android.location.Location

class Pockemon {

    var name: String? = null
    var image: Int? = null
    var desc: String? = null
    var power: Double? = null
    var isCatch: Boolean? = false
    var location: Location? = null

    constructor(name: String, image: Int, desc: String, lat:Double, log: Double, power: Double){
        this.name = name
        this.image = image
        this.desc = desc
        this.location = Location(name)
        this.location!!.latitude = lat
        this.location!!.longitude = log
        this.power = power
        this.isCatch = false
    }
}