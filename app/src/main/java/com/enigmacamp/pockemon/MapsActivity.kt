package com.enigmacamp.pockemon

import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions


class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        checkPermission()
        LoadPockemon()
    }

    var ACCERLOCATION = 123
    fun checkPermission(){
        if(Build.VERSION.SDK_INT >= 23){

            if(ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
                requestPermissions(arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION), ACCERLOCATION)
                return
            }

        }

        GetLocation()
    }

    fun GetLocation(){
        Toast.makeText(this, "User location on", Toast.LENGTH_LONG).show()

        var myLocation =MyLocation()

        var locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager

        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 3, 3f, myLocation)

        var mythread = myThread()
        mythread.start()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {

        when(requestCode){
            ACCERLOCATION -> {
                if(grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    GetLocation()
                } else {
                    Toast.makeText(this, "We cannot acces your location", Toast.LENGTH_LONG).show()
                }
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        // Add a marker in Sydney and move the camera

    }

    fun resizeBitmap(drawableName: String?, width: Int, height: Int): Bitmap? {
        val imageBitmap = BitmapFactory.decodeResource(
            resources,
            resources.getIdentifier(drawableName, "drawable", packageName)
        )
        return Bitmap.createScaledBitmap(imageBitmap, width, height, false)
    }

    var location:Location? = null

    inner class MyLocation: LocationListener{

        constructor(){
            location = Location("Start")
            location!!.latitude = -6.330212
            location!!.longitude = 106.826378
        }

        override fun onLocationChanged(p0: Location?) {
            location = p0
        }

        override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {
           // TODO("Not yet implemented")
        }

        override fun onProviderEnabled(provider: String?) {
            // TODO("Not yet implemented")
        }

        override fun onProviderDisabled(provider: String?) {
            //TODO("Not yet implemented")
        }


    }

    var oldLocation: Location? = null
    inner class myThread: Thread{

        constructor(): super(){
            oldLocation = Location("Start")
            oldLocation!!.latitude = -6.330212
            oldLocation!!.longitude = 106.826378

        }

        override fun run() {

            while (true){
                try {

                    if(oldLocation!!.distanceTo(location) == 0f){
                        continue
                    }

                    oldLocation=location

                    runOnUiThread {

                        mMap.clear()

                        //show me
                        Log.d("lat", location!!.latitude.toString())
                        val jakarta = LatLng(location!!.latitude, location!!.longitude)
                        mMap.addMarker(
                            MarkerOptions()
                                .position(jakarta)
                                .title("Me")
                                .snippet(" here is my location")
                                .icon(
                                    BitmapDescriptorFactory.fromBitmap(
                                        resizeBitmap(
                                            R.drawable.mario.toString(),
                                            150,
                                            180
                                        )
                                    )
                                )
                        )
                        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(jakarta, 8f))

                        //show pockemon
                        for(i in 0..listPockemons.size-1){
                            var newPockemon = listPockemons[i]

                            if(newPockemon.isCatch == false){
                                val pockemonLoc = LatLng(newPockemon.location!!.latitude, newPockemon.location!!.longitude)
                                mMap.addMarker(
                                    MarkerOptions()
                                        .position(pockemonLoc)
                                        .title(newPockemon.name!!)
                                        .snippet(newPockemon.desc!! + ", Power: "+newPockemon.power!!)
                                        .icon(
                                            BitmapDescriptorFactory.fromBitmap(
                                                resizeBitmap(
                                                    newPockemon.image!!.toString(),
                                                    100,
                                                    130
                                                )
                                            )
                                        )
                                )

                                if(location!!.distanceTo(newPockemon.location) < 2){
                                    newPockemon.isCatch = true
                                    listPockemons[i] = newPockemon
                                    playerPower += newPockemon.power!!
                                    Toast.makeText(applicationContext, "You catch new pockemon your new power is "+playerPower, Toast.LENGTH_LONG).show()

                                }
                            }
                        }
                    }

                    Thread.sleep(1000)
                }catch (ex: Exception){

                }
            }
        }
    }

    var playerPower = 0.0
    var listPockemons = ArrayList<Pockemon>()

    fun LoadPockemon(){
        listPockemons.add(Pockemon("Pikacu", R.drawable.pokemon2, "Pikacu from Bekasi", -6.237689, 107.056816, 55.0))
        listPockemons.add(Pockemon("Pokemon", R.drawable.pokemon3, "Pokemon from Jakarta", -6.121435, 106.774124, 55.0))
    }
}
